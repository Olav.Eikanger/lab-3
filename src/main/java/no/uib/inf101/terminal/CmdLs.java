package no.uib.inf101.terminal;

import java.io.File;

public class CmdLs implements Command {
    private Context context;

    private enum IgnoreMode {
        IGNORE_DEFAULT,
        IGNORE_MINIMAL
    }

    private enum Format {
        LONG_FORMAT,
        MANY_PER_LINE
    }

    private boolean fileIgnored(IgnoreMode mode, File file) {
        return (
            (mode != IgnoreMode.IGNORE_MINIMAL && file.isHidden())
            );
    }

    private String createList(Format format, IgnoreMode mode, File[] files) {
        String seperator;
        switch (format) {
            case LONG_FORMAT:
                seperator = "\n";
                break;
            case MANY_PER_LINE:
            default:
                seperator = " ";
                break;
        }

        String s = "";
        for (File file : files) {
            if (fileIgnored(mode, file)) {
                continue;
            }

            s += file.getName();
            s += seperator;
        }
        return s;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        /* Set default perameters */
        Format format = Format.MANY_PER_LINE;
        IgnoreMode ignoreMode = IgnoreMode.IGNORE_DEFAULT;
        File targetDirectory = this.context.getCwd();


        /* Apply options and arguments given */
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                char[] options = args[i].toCharArray();
                for (int j = 1; j < options.length; j++) {
                    switch (options[j]) {
                        case 'a':
                            ignoreMode = IgnoreMode.IGNORE_MINIMAL;
                            break;
                        case 'l':
                            format = Format.LONG_FORMAT;
                            break;
                        default:
                            return "Invalid option: " + options[j];
                    }
                }
            } else if (i == args.length - 1) {
                targetDirectory = new File(args[i]);
            } else {
                return "Invalid option: " + args[i];
            }
        }

        return createList(format, ignoreMode, targetDirectory.listFiles());
    }

    @Override
    public String getName() {
        return "ls";
    }

    @Override
    public String getManual() {
        return """
                Lists all files and folders in the given directory.
                (If no directory is given, current working directory is assumed).
                """;
    }
    
}
