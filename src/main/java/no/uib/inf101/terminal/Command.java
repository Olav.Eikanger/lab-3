package no.uib.inf101.terminal;

import java.util.Map;

public interface Command {
    default void setContext(Context context) { /* do nothing */ };

    default void setCommandContext(Map<String, Command> commandContexts) { /* do nothing */};

    public String run(String args[]);

    public String getName();

    public String getManual();
}
