package no.uib.inf101.terminal;

import java.io.File;
import java.io.IOException;

public class CmdTouch implements Command {

    @Override
    public String run(String[] args) {
        File file = new File(args[0]);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                return "Could not create file: " + args[0];
            }
        }
        return args[0];
    }

    @Override
    public String getName() {
        return "touch";
    }

    @Override
    public String getManual() {
        return """
                Creates a new empty file with the given name
                if it doesn't already exist.
                """;
    }
    
}
