package no.uib.inf101.terminal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CmdCat implements Command {

    @Override
    public String run(String[] args) {
        File file = new File(args[0]);
        String s = "";
        if (file.isFile()) {
            try {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNext()) {
                    s += scanner.nextLine();
                    s += '\n';
                }
                scanner.close();
            } catch (FileNotFoundException e) {
            }
        }
        return s;
    }

    @Override
    public String getName() {
        return "cat";
    }

    @Override
    public String getManual() {
        return """
                Prints the content of a file.
                """;
    }
    
}
