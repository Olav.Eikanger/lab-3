package no.uib.inf101.terminal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class CmdFgrep implements Command {
    private Context context;

    private enum FileSearchOption {
        CASE_INSENSITIVE,
        INVERSE,
        INCLUDE_LINE_NUMBER,
        COUNT_LINES
    }

    private enum DirectorySearchOption {
        NAME_OF_FILES_WITH_MATCH,
        NAME_OF_FILES_WITHOUT_MATCH,
        INCLUDE_LINE_NUMBER,
        RECURSIVE
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }
    
    @Override
    public String run(String[] args) {
        String searchword = args[0];
        String filename = args[1];
        List<FileSearchOption> fileSearchOptions = new LinkedList<>();
        List<DirectorySearchOption> directorySearchOptions = new LinkedList<>();
        for (int i = 2; i < args.length; i++) {
            String[] seperatedArgs = args[i].split("");
            for (int j = 1; j < seperatedArgs.length; j++) {
                switch (seperatedArgs[j]) {
                    case "c":
                        fileSearchOptions.add(FileSearchOption.COUNT_LINES);
                        break;
                    case "i":
                        fileSearchOptions.add(FileSearchOption.CASE_INSENSITIVE);
                        break;
                    case "l":
                        if (!directorySearchOptions.contains(DirectorySearchOption.NAME_OF_FILES_WITHOUT_MATCH)) {
                            directorySearchOptions.add(DirectorySearchOption.NAME_OF_FILES_WITH_MATCH);
                        }
                        break;
                    case "L":
                        if (!directorySearchOptions.contains(DirectorySearchOption.NAME_OF_FILES_WITH_MATCH)) {
                            directorySearchOptions.add(DirectorySearchOption.NAME_OF_FILES_WITHOUT_MATCH);
                        }
                        break;
                    case "n":
                        fileSearchOptions.add(FileSearchOption.INCLUDE_LINE_NUMBER);
                        break;
                    case "r":
                        directorySearchOptions.add(DirectorySearchOption.RECURSIVE);
                        break;
                    case "v":
                        fileSearchOptions.add(FileSearchOption.INVERSE);
                        break;
                    default:
                        return "Could not recoginize option: " + seperatedArgs[j];
                }
            }
        }
        
        List<String> result = new LinkedList<>();
        if (filename.equals("*")) {
            result = searchDirectory(this.context.getCwd(), searchword, directorySearchOptions);
        }
        
        File targetFile = new File(filename);
        if (!targetFile.exists()) {
            return "The path " + filename + " does not exist";
        }

        if (targetFile.isDirectory()) {
            result = searchDirectory(targetFile, searchword, directorySearchOptions);
        } else if (targetFile.isFile()) {
            result = searchSingleFile(targetFile, searchword, fileSearchOptions);
            if (fileSearchOptions.contains(FileSearchOption.COUNT_LINES)) {
                return Integer.toString(result.size());
            }
        }

        String s = "";
        for (String line : result) {
            s += line;
            s += '\n';
        }

        return s;
    }

    private List<String> searchSingleFile(File file, String searchword, List<FileSearchOption> options) {
        boolean inverse = false;
        boolean caseSensitive = true;
        boolean includeLineNumber = false;
        for (FileSearchOption option : options) {
            switch (option) {
                case INVERSE:
                    inverse = true; 
                    break;
                case CASE_INSENSITIVE:
                    caseSensitive = false;
                    break;
                case INCLUDE_LINE_NUMBER:
                    includeLineNumber = true;
                    break;
                case COUNT_LINES:
                    break;
            }
        }

        List<String> list = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(file);
            int i = 0;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                boolean match = line.contains(searchword);
                boolean caseInsesitiveMatch = line.toLowerCase().contains(searchword.toLowerCase());
                if ((match || (caseInsesitiveMatch && !caseSensitive)) ^ inverse) {
                    if (includeLineNumber) {
                        line = i + " " + line;
                    }

                    list.add(line);
                }

                i++;
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            // Should never happen
        }

        return list;
    }

    private List<String> searchDirectory(File directory, String searchword, List<DirectorySearchOption> options) {
        boolean recursive = false;
        boolean printNameOnly = false;
        boolean inverse = false;
        List<FileSearchOption> fileSearchOptions = new LinkedList<>();
        for (DirectorySearchOption option : options) {
            switch (option) {
                case NAME_OF_FILES_WITHOUT_MATCH:
                    inverse = true;
                    /* Overflows */
                case NAME_OF_FILES_WITH_MATCH:
                    printNameOnly = true;
                    break;
                case RECURSIVE:
                    recursive = true;
                    break;
                case INCLUDE_LINE_NUMBER:
                    break;
            }
        }

        List<String> list = new LinkedList<>();
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                if (recursive) {
                    list.addAll(searchDirectory(file, searchword, options));
                }

                continue;
            }

            List<String> occureances = this.searchSingleFile(file, searchword, fileSearchOptions);
            if (occureances.size() > 0 && inverse) {
                continue;
            }

            if (printNameOnly) {
                list.add(file.getName());
                continue;
            }

            list.addAll(occureances);
        }
        return list;
    }

    @Override
    public String getName() {
        return "fgrep";
    }

    @Override
    public String getManual() {
        return """
                Searches through a file for the keyword
                """;
    }
    
}
