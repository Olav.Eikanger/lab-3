package no.uib.inf101.terminal;

import java.io.File;
import java.io.FileFilter;

public class ListOptions {
    private String seperator;
    private FileFilter filter;
    private File directory;

    ListOptions(String s, FileFilter f, File d) {
        this.seperator = s;
        this.filter = f;
        this.directory = d;
    }

    public void setSeperator(String s) {
        this.seperator = s;
    }

    public String getSeperator() {
        return this.seperator;
    }

    public void setFilter(FileFilter f) {
        this.filter = f;
    }

    public FileFilter getFilter() {
        return this.filter;
    }

    public void setDirectory(File d) {
        this.directory = d;
    }

    public File getDirectory() {
        return this.directory;
    }
}
