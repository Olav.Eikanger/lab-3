package no.uib.inf101.terminal;

import java.util.Map;
import java.util.Objects;

public class CmdMan implements Command {
    private Map<String, Command> commands;

    @Override
    public void setCommandContext(Map<String, Command> commandContexts) {
        this.commands = commandContexts;
    }

    @Override
    public String run(String[] args) {
        Command command = commands.get(args[0]);
        if (Objects.equals(command, null)) {
            return null;
        }
        return command.getManual();
    }

    @Override
    public String getName() {
        return "man";
    }

    @Override
    public String getManual() {
        return """
                Prints the manual for the given command.
                """;
    }
    
}
