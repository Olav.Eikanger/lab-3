package no.uib.inf101.terminal;

import java.io.File;

public class CmdMkDir implements Command {

    @Override
    public String run(String[] args) {
        File directory = new File(args[0]);
        if (directory.isFile()) {
            return "Could not create directory " + args[0] + ", a file already exists";
        } else if (!directory.exists()) {
            directory.mkdirs();
        }
        return args[0];
    }

    @Override
    public String getName() {
        return "mkdir";
    }

    @Override
    public String getManual() {
        return """
                Creates a directory with the given name
                if it does not already exist.
                """;
    }
    
}
