package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    @Override
    public String run(String[] args) {
        String line = "";
        for (String arg : args) {
            line += arg + " ";
        }
        return line;
    }

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String getManual() {
        return """
                Prints the arguments given to the terminal seperated
                by spaces (\\s).
                """;
    }
}
